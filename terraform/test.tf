module "separate-test-terraform" {
  source = "./modules/"
  ami = "ami-0c667680ab0ae9def"
  instance_type = "t2.small"
  env_name = "terraform-separate-test"
  pub_subnet_ids = ["subnet-085e94774a59f25ae","subnet-07161232a0dbde413"]
  pri_subnet_ids = ["subnet-05c2cc8456c4330c3","subnet-0ebc4841acad139bf"]
  vpc_id = "vpc-0a07901f1ef353d4b"
}
