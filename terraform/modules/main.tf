terraform {
  required_version = "= 0.12.6"

  backend "s3" {
    bucket  = "dojo-sero-separate-terraform-state"
    key     = "terraform.tfstate"
    region  = "ap-northeast-1"
    profile = "dojo-sero"
  }
}

provider "aws" {
  region  = "ap-northeast-1"
  version = "2.20.0"
  profile = "dojo-sero"
}

provider "null" {
  version = "~> 2.1"
}

variable "env_name" {}
variable "ami" {}
variable "instance_type" {}
variable "vpc_id" {}
variable "pub_subnet_ids" {}
variable "pri_subnet_ids" {}


resource "aws_iam_instance_profile" "dojo-sero-separate" {
  name = "${var.env_name}"
  role = "DojoSeroSeparateRole"
}

# resource "aws_security_group" "db" {
#   name ="sepa-env-db-security"
#   vpc_id = "${var.vpc_id}"
# }

 resource "aws_db_subnet_group" "separate-terra-db-subnet" {
     name = "separate-env-db-subnet"
     subnet_ids = "${var.pri_subnet_ids}"
 }

 resource "aws_db_instance" "sepa-db-terra" {
    allocated_storage = 20
    engine = "postgres"
    engine_version = "10.6"
    instance_class       = "db.m5.large"
    username             = "postgres"
    password             = "postgres"
    identifier = "${var.env_name}"
    name = "d4erp"
    vpc_security_group_ids = ["${var.vpc_id}"]
    db_subnet_group_name = "separate-terra-db-subnet"
 }
resource "aws_lb" "separate-alb" {
  name = "${var.env_name}"
  internal = true
  load_balancer_type = "application"
  security_groups = [
    "vpc-0a07901f1ef353d4b",
    "vpc-0d41a67e3215805ad"
  ]
  subnets = "${var.pub_subnet_ids}"
  ip_address_type = "ipv4"
}

resource "aws_lb_target_group" "separate-group-terraform" {
  name = "fujimoto-sepa-test-gloup"
  port = 80
  protocol = "HTTP"
  vpc_id = "${var.vpc_id}"
  health_check {
    interval            = 30
    path                = "/health_check.html"
    port                = 80
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 2
    matcher             = 200
  }
}
resource "aws_launch_configuration" "sepa-test" {
    name = "sepa-test"
    image_id = "${var.ami}"
    instance_type = "${var.instance_type}"
    associate_public_ip_address = true
    lifecycle {
        create_before_destroy = true
    }
}
resource "aws_autoscaling_group" "terraform-sepa-test" {
  name = "foo"
  max_size = 4
  min_size = 1
  desired_capacity = 1
  health_check_grace_period = 300
  health_check_type = "ELB"
  force_delete = true
  launch_configuration = "${aws_launch_configuration.sepa-test.image_id}"
  vpc_zone_identifier = "${var.pub_subnet_ids}"
  load_balancers = ["${aws_lb.separate-alb.name}"]
}

